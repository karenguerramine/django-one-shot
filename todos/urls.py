from django.urls import path
from . import views

urlpatterns = [
    path('', views.todo_list_list, name="todo_list_list"),
    path('<int:pk>/', views.todo_list_detail, name="todo_list_detail"),
    path('create/', views.todo_list_create, name="todo_list_create")
]