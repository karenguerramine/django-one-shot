from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {'todo_list': todo_list}
    return render(request, 'todos/list.html', context)


def todo_list_detail(request, pk):
    todo_task = TodoList.objects.get(id=pk)
    context = {'todo_task': todo_task}
    return render(request, 'todos/detail.html', context)


def todo_list_create(request):
    form = TodoListForm
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail') #getting no reverse match, need to redirect to the detail page of the created todolist???
            
    context = {'form': form}
    return render(request, 'todos/create.html', context)