from django import forms
from .models import TodoList, TodoItem

class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = "__all__"