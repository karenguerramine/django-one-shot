# Generated by Django 4.1.1 on 2022-09-07 19:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0003_rename_task_list_todoitem_list"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="is_completed",
            field=models.BooleanField(null=True),
        ),
    ]
